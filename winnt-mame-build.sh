#!/bin/bash

# this may hog memory, set nproc accordingly
git clone https://github.com/hyenasky/mame-r3kjazz.git
cd mame-r3kjazz
make -j6 SUBTARGET=r3000jazz SOURCES=src/mame/microsoft/r3kjazz.cpp

# ROMs consist of j3prom.bin (unique to r3kjazz) + natural.bin and 72x8455.zm82
cd roms
wget https://cdn.discordapp.com/attachments/1171434091384672326/1173601079934603326/r3000jazz.tar.gz
tar -xvf r3000jazz.tar.gz
cd ..

# disk image for standalone NTSerialTool
wget https://cdn.discordapp.com/attachments/1171434091384672326/1173587256594944000/nt.raw

# alternative disk image for OS/2 mipskd
# wget https://cdn.discordapp.com/attachments/1171434091384672326/1173556346596098058/nt.raw

# To start MAME, run r3000jazz.sh
ln -s ../r3000jazz.sh .
chmod +x r3000jazz.sh

# If MAME NVRAM is blank...
# in Arc, Run Setup -> Initialize System -> Set default configuration (take the defaults) -> Set default environment (same thing) -> Return to main menu -> Exit

# in Arc, Run a program -> run "ntoskrnl" (at prompt)
